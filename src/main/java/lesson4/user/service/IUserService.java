package lesson4.user.service;

import lesson4.user.dto.UserLoginDto;
import lesson4.user.dto.UserPasswordResetDto;
import lesson4.user.dto.UserRegisterDto;
import lesson4.user.entity.User;

public interface IUserService {
    public void registerUser(UserRegisterDto UserRegisterDto);
    public boolean loginUser(UserLoginDto userLoginDto);
    public void resetPassword(UserPasswordResetDto passwordResetDto);
    public User getOneUserByUserName(String username);
    public User getOneUserByEmail(String email);
    public boolean validatePassword(UserRegisterDto UserRegisterDto);
    public boolean validateVerificationEmail(UserPasswordResetDto passwordResetDto);
    public boolean validateVerificationCode(UserPasswordResetDto passwordResetDto);
}
