package lesson4.user.controller;

import lesson4.user.dto.UserLoginDto;
import lesson4.user.dto.UserPasswordResetDto;
import lesson4.user.dto.UserRegisterDto;
import lesson4.user.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final IUserService service;

    public UserController(IUserService service) {
        this.service = service;
    }

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody UserRegisterDto createUserDto) {
        if (service.getOneUserByUserName(createUserDto.getUsername()) != null) {
            return new ResponseEntity<>("Username already in use!", HttpStatus.BAD_REQUEST);
        }
        if (service.getOneUserByEmail(createUserDto.getEmail()) != null) {
            return new ResponseEntity<>("Email already in use", HttpStatus.BAD_REQUEST);
        }
        if (!service.validatePassword(createUserDto)) {
            return new ResponseEntity<>("Password is different from Confirm Password", HttpStatus.BAD_REQUEST);
        }
        service.registerUser(createUserDto);
        return new ResponseEntity<>("You Are Successfully Registered", HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody UserLoginDto userLoginDto) {
        if (service.getOneUserByUserName(userLoginDto.getUsername()) == null) {
            return new ResponseEntity<>("Username not found", HttpStatus.NOT_FOUND);
        }
        if (!service.loginUser(userLoginDto)) {
            return new ResponseEntity<>("Unauthorized Login", HttpStatus.UNAUTHORIZED);
        } else return new ResponseEntity<>("You Are Successfully Login", HttpStatus.OK);
    }

    @PutMapping("/resetPasswordByEmail")
    public ResponseEntity<String> resetPasswordByVerificationEmail(@RequestBody UserPasswordResetDto passwordResetDto) {
        if (!service.validateVerificationEmail(passwordResetDto)) {
            return new ResponseEntity<>("Verification Email is incorrect", HttpStatus.NOT_FOUND);
        }
        if (!passwordResetDto.getPassword().equals(passwordResetDto.getConfirmPassword())) {
            return new ResponseEntity<>("Password is different from Confirm Password", HttpStatus.BAD_REQUEST);
        }
        service.resetPassword(passwordResetDto);
        return new ResponseEntity<>("Password changed successfully",HttpStatus.OK);
    }

    @PutMapping("/resetPasswordByCode")
    public ResponseEntity<String> resetPasswordByVerificationCode(@RequestBody UserPasswordResetDto passwordResetDto) {
        if (!service.validateVerificationCode(passwordResetDto)) {
            return new ResponseEntity<>("Verification Code is incorrect", HttpStatus.NOT_FOUND);
        }
        if (!passwordResetDto.getPassword().equals(passwordResetDto.getConfirmPassword())) {
            return new ResponseEntity<>("Password is different from Confirm Password", HttpStatus.BAD_REQUEST);
        }
        service.resetPassword(passwordResetDto);
        return new ResponseEntity<>("Password changed successfully",HttpStatus.OK);
    }


}
