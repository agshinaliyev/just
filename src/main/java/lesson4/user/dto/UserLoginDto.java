package lesson4.user.dto;

import lombok.Data;

@Data
public class UserLoginDto {
    private String username;
    private String password;
}
