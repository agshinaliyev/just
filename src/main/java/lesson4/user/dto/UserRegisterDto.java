package lesson4.user.dto;

import lombok.Data;

@Data
public class UserRegisterDto {
    private String username;
    private String password;
    private String confirmPassword;
    private String email;
    private String verificationEmail;
    private String verificationCode;
}
